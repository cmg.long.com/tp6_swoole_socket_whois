<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'topthink/think',
  ),
  'versions' => 
  array (
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.70',
      'version' => '1.0.70.0',
      'aliases' => 
      array (
      ),
      'reference' => '585824702f534f8d3cf7fab7225e8466cc4b7493',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
    ),
    'nette/php-generator' => 
    array (
      'pretty_version' => 'v3.5.4',
      'version' => '3.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '59bb35ed6e8da95854fbf7b7d47dce6156b42915',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c8d1628317fddc692d90fd7732e3dd98327dbf0',
    ),
    'open-smf/connection-pool' => 
    array (
      'pretty_version' => 'v1.0.16',
      'version' => '1.0.16.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f70e47dbf56f1869d3207e15825cf38810b865e0',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'stechstudio/backoff' => 
    array (
      'pretty_version' => '1.2',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '816e46107a6be2e1072ba0ff2cb26034872dfa49',
    ),
    'swoole/ide-helper' => 
    array (
      'pretty_version' => '4.8.11',
      'version' => '4.8.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edcf955501271b1fd5d77efef31027b1e08f4a85',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.41',
      'version' => '4.4.41.0',
      'aliases' => 
      array (
      ),
      'reference' => '40790bdf293b462798882ef6da72bb49a4a6633a',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf44a9fd41feaac72b074de600314a93e2ae78e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa0ae98841b9e461207c13ab093d76b0fa7bace',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.42',
      'version' => '4.4.42.0',
      'aliases' => 
      array (
      ),
      'reference' => '742aab50ad097bcb62d91fccb613f66b8047d2ca',
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v6.0.8',
      'version' => '6.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '4789343672aef06d571d556da369c0e156609bce',
    ),
    'topthink/think' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'topthink/think-orm' => 
    array (
      'pretty_version' => 'v2.0.53',
      'version' => '2.0.53.0',
      'aliases' => 
      array (
      ),
      'reference' => '06783eda65547a70ea686360a897759e1f873fff',
    ),
    'topthink/think-swoole' => 
    array (
      'pretty_version' => 'v3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df78b1f6eb6cd8f45f49ab7b0d4cc65595181504',
    ),
    'topthink/think-trace' => 
    array (
      'pretty_version' => 'v1.4',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
    ),
    'yurunsoft/yurun-http' => 
    array (
      'pretty_version' => 'v4.3.12',
      'version' => '4.3.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0f50f4024da89f62e7e38a4faa9096483032349',
    ),
  ),
);
